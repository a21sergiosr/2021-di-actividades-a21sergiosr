package com.mycompany.ejercicio2layout;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/* FrameDemo.java requires no other files. */
public class ejercicio2layout {

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Ejercicio 2");

        Panel p = new Panel();
        p.setLayout(new FlowLayout());

        Panel p2 = new Panel();
        p2.setLayout(new FlowLayout());

        Panel p3 = new Panel();
        p3.setLayout(new CardLayout());

        JLabel lbl = new JLabel("Titulo");
        JLabel lbl2 = new JLabel("Sergio");
        JButton btn = new JButton("Anterior");
        JButton btn2 = new JButton("Color1");
        JButton btn3 = new JButton("Color2");
        JButton btn4 = new JButton("Color3");
        JButton btn5 = new JButton("Siguiente color");
        JButton btn6 = new JButton("-");
        JButton btn7 = new JButton("-");
        JButton btn8 = new JButton("-");
        JButton btn9 = new JButton("-");
        JButton btn10 = new JButton("-");

        JComboBox cmb = new JComboBox();
        cmb.addItem("red");
        cmb.addItem("blue");
        cmb.addItem("green");
        p.add(btn);
        p.add(btn2);
        p.add(btn3);
        p.add(btn4);
        p.add(btn5);
        p2.add(btn6);
        p2.add(btn7);
        p2.add(btn8);
        p2.add(btn9);
        p2.add(btn10);

        p3.add(cmb);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.pack();
        frame.getContentPane().add(p, BorderLayout.WEST);
        frame.getContentPane().add(p2, BorderLayout.EAST);
        frame.getContentPane().add(p3, BorderLayout.CENTER);
        frame.getContentPane().add(lbl, BorderLayout.NORTH);
        frame.getContentPane().add(lbl2, BorderLayout.SOUTH);

        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl2.setHorizontalAlignment(JLabel.CENTER);
        frame.setLocationRelativeTo(null);
        frame.setSize(1100, 100);
        frame.setVisible(true);

        cmb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int color = cmb.getSelectedIndex();
                if (color == 0) {

                    frame.getContentPane().setBackground(Color.red);
                }
                if (color == 1) {

                    frame.getContentPane().setBackground(Color.blue);
                }
                if (color == 2) {

                    frame.getContentPane().setBackground(Color.green);
                }

            }
        });

        btn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int color = cmb.getSelectedIndex();

                if (color == 0) {

                    frame.getContentPane().setBackground(Color.green);
                }
                if (color == 1) {

                    frame.getContentPane().setBackground(Color.red);
                }
                if (color == 2) {

                    frame.getContentPane().setBackground(Color.blue);
                }
            }
        });
        btn2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                frame.getContentPane().setBackground(Color.red);

            }
        });
        
        btn3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                frame.getContentPane().setBackground(Color.blue);

            }
        });
        
        btn4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                frame.getContentPane().setBackground(Color.green);

            }
        });
        
        btn5.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int color = cmb.getSelectedIndex();

                if (color == 0) {

                    frame.getContentPane().setBackground(Color.blue);
                }
                if (color == 1) {

                    frame.getContentPane().setBackground(Color.green);
                }
                if (color == 2) {

                    frame.getContentPane().setBackground(Color.red);
                }
            }
        });

    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
