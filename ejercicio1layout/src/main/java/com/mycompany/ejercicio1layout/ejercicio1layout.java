package com.mycompany.ejercicio1layout;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/* FrameDemo.java requires no other files. */
public class ejercicio1layout {

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Ejercicio 1");

        Panel p = new Panel();
        p.setLayout(new FlowLayout());

        Panel p2 = new Panel();
        p2.setLayout(new FlowLayout());

        JLabel lbl = new JLabel("Titulo");
        JLabel lbl2 = new JLabel("Sergio");
        JTextArea txa = new JTextArea();
        p.add(new Button("Okay"));
        p.add(new Button("Okay2"));
        p.add(new Button("Okay3"));
        p.add(new Button("Okay4"));
        p.add(new Button("Okay5"));

        p2.add(new Button("Okay6"));
        p2.add(new Button("Okay7"));
        p2.add(new Button("Okay8"));
        p2.add(new Button("Okay9"));
        p2.add(new Button("Okay10"));

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.pack();
        frame.getContentPane().add(p, BorderLayout.WEST);
        frame.getContentPane().add(p2, BorderLayout.EAST);
        frame.getContentPane().add(lbl, BorderLayout.NORTH);
        frame.getContentPane().add(lbl2, BorderLayout.SOUTH);
        frame.getContentPane().add(txa, BorderLayout.CENTER);
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl2.setHorizontalAlignment(JLabel.CENTER);
        frame.setLocationRelativeTo(null);
        frame.setSize(700, 200);
        frame.setVisible(true);

    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
