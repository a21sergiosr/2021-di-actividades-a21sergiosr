package com.mycompany.ejerciciocalculadora;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/* FrameDemo.java requires no other files. */
public class calculadora {

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Ejercicio 5");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new BorderLayout());
        JTextField txtf=new JTextField();
        Panel p = new Panel();
        p.setLayout(new GridLayout(4, 4));
        p.add(new Button("1"));
        p.add(new Button("2"));
        p.add(new Button("3"));
        p.add(new Button("/"));
        p.add(new Button("4"));
        p.add(new Button("5"));
        p.add(new Button("6"));
        p.add(new Button("*"));
        p.add(new Button("7"));
        p.add(new Button("8"));
        p.add(new Button("9"));
        p.add(new Button("-"));
        p.add(new Button("0"));
        p.add(new Button("."));
        p.add(new Button("="));
        p.add(new Button("+"));
  
        
        frame.pack();
        
        frame.getContentPane().add(p, BorderLayout.CENTER);
     frame.getContentPane().add(txtf, BorderLayout.NORTH);
        frame.setLocationRelativeTo(null);
        frame.setSize(300, 200);
        frame.setVisible(true);

    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
