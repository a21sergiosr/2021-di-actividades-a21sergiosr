package com.mycompany.ejercicio5combobox;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/* FrameDemo.java requires no other files. */
public class ejercicio5combobox {

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    private static void createAndShowGUI() {

        JFrame frame = new JFrame("Ejercicio 5");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lbl = new JLabel("R");
        JLabel lbl1 = new JLabel("G");
        JLabel lbl2 = new JLabel("B");

        JComboBox cmb = new JComboBox();
        JComboBox cmb2 = new JComboBox();
        JComboBox cmb3 = new JComboBox();

        for (int f = 0; f <= 255; f++) {
            cmb.addItem(String.valueOf(f));
        }
        for (int f = 0; f <= 255; f++) {
            cmb2.addItem(String.valueOf(f));
        }
        cmb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cad1 = (String) cmb.getSelectedItem();
                String cad2 = (String) cmb2.getSelectedItem();
                String cad3 = (String) cmb3.getSelectedItem();
                int rojo = Integer.parseInt(cad1);
                int verde = Integer.parseInt(cad2);
                int azul = Integer.parseInt(cad3);
                Color color1 = new Color(rojo, verde, azul);
                frame.getContentPane().setBackground(color1);
            }
        });

        for (int f = 0; f <= 255; f++) {
            cmb3.addItem(String.valueOf(f));
        }

        //
        frame.getContentPane().add(lbl, BorderLayout.CENTER);
        frame.getContentPane().add(cmb, BorderLayout.CENTER);
        //
        frame.getContentPane().add(lbl1, BorderLayout.CENTER);
        frame.getContentPane().add(cmb2, BorderLayout.CENTER);
        //
        frame.getContentPane().add(lbl2, BorderLayout.CENTER);
        frame.getContentPane().add(cmb3, BorderLayout.CENTER);

        frame.setLayout(new FlowLayout());
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setSize(300, 200);
        frame.setVisible(true);

        cmb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cad1 = (String) cmb.getSelectedItem();
                String cad2 = (String) cmb2.getSelectedItem();
                String cad3 = (String) cmb3.getSelectedItem();
                int rojo = Integer.parseInt(cad1);
                int verde = Integer.parseInt(cad2);
                int azul = Integer.parseInt(cad3);
                Color color1 = new Color(rojo, verde, azul);
                frame.getContentPane().setBackground(color1);
            }
        });
        cmb2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cad1 = (String) cmb.getSelectedItem();
                String cad2 = (String) cmb2.getSelectedItem();
                String cad3 = (String) cmb3.getSelectedItem();
                int rojo = Integer.parseInt(cad1);
                int verde = Integer.parseInt(cad2);
                int azul = Integer.parseInt(cad3);
                Color color1 = new Color(rojo, verde, azul);
                frame.getContentPane().setBackground(color1);
            }
        });

        cmb3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cad1 = (String) cmb.getSelectedItem();
                String cad2 = (String) cmb2.getSelectedItem();
                String cad3 = (String) cmb3.getSelectedItem();
                int rojo = Integer.parseInt(cad1);
                int verde = Integer.parseInt(cad2);
                int azul = Integer.parseInt(cad3);
                Color color1 = new Color(rojo, verde, azul);
                frame.getContentPane().setBackground(color1);
            }
        });

    }

    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
